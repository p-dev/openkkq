using System;
using System.Collections.Generic;
using OpenKkq.Server.Models.Items;

namespace OpenKkq.Server.Models
{
    public class User
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        public string Password { get; set; }
        
        public ICollection<Item> Items { get; set; }
    }
}