namespace OpenKkq.Server.Models.Items
{
    public class Kolbasa : Item
    {
        public Kolbasa()
        {
            
        }
        
        public KolbasaKind Kind { get; set; }

        public bool IsPapered { get; set; }
        public bool IsSalted { get; set; }

        private const int PaperedPrice = 2;
        private const int SaltedPrice = 4;
        
        public override int Price 
            => Kind.Price 
               + (IsPapered ? PaperedPrice : 0) 
               + (IsSalted ? SaltedPrice : 0);
    }
}