using System;

namespace OpenKkq.Server.Models.Items
{
    public class KolbasaKind
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public int Price { get; set; }
    }
}