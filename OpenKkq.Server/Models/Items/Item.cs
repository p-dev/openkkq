using System;
using System.ComponentModel.DataAnnotations;

namespace OpenKkq.Server.Models.Items
{
    public abstract class Item
    {
        [Key]
        public Guid Id { get; set; }
        
        public User Owner { get; set; }
        
        public abstract int Price { get; }
    }
}