using System;

namespace OpenKkq.Server.ViewModels
{
    public class TokenResponse
    {
        public string Token { get; set; }
    }
}