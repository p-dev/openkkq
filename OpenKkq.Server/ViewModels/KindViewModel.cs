namespace OpenKkq.Server.ViewModels
{
    public class KindViewModel
    {
        public string Name { get; set; }
        public int Price { get; set; }
    }
}