using System.ComponentModel.DataAnnotations;

namespace OpenKkq.Server.ViewModels
{
    public class TokenRequest
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Password { get; set; }
    }
}