using System;

namespace OpenKkq.Server.ViewModels
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}