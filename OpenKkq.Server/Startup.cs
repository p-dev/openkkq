﻿using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using OpenKkq.Server.Models;
using OpenKkq.Server.Services;

namespace OpenKkq.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddTransient<IKolbasaService, KolbasaService>();
            services.AddTransient<IUserManagementService, UserManagementService>();

            var connectionString = Configuration.GetConnectionString("KkqDbSlite");
            services.AddDbContext<KkqDbContext>(builder => builder.UseSqlite(connectionString));

            // Auth

            services.AddTransient<IAuthenticateService, TokenAuthenticationService>();
            services.Configure<TokenManagement>(Configuration.GetSection(nameof(TokenManagement)));
//            services.AddOptions<TokenManagement>()
            
            var token = Configuration.GetSection(nameof(TokenManagement)).Get<TokenManagement>();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(token.Secret)),
                    ValidateIssuer = false,
                    ValidIssuer = "openkkq",
                    ValidateAudience = false,
                    ValidAudience = "openkkq-client",
//                    ValidateLifetime = true,
//                    ClockSkew = TimeSpan.FromSeconds(5)
                };
            });

            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();

//            app.UseCors(builder => builder.AllowAnyOrigin());
            
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
