using OpenKkq.Server.Models;
using OpenKkq.Server.ViewModels;

namespace OpenKkq.Server.Services
{
    public interface IAuthenticateService
    {
        bool IsAuthenticated(TokenRequest request, out TokenResponse token);

        User NewUser(TokenRequest request);
    }
}