using System;
using System.Collections.Generic;
using System.Linq;
using OpenKkq.Server.Models;

namespace OpenKkq.Server.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly KkqDbContext _db;

        public UserManagementService(KkqDbContext db)
        {
            _db = db;
        }

        public User GetUserById(Guid id)
        {
            return _db.Users.SingleOrDefault(user => user.Id == id);
        }

        public User GetUserByName(string name)
        {
            return _db.Users.SingleOrDefault(x => x.Name == name);
        }
        
        public bool IsValidUser(string name, string password, out User user)
        {
            user = _db.Users.SingleOrDefault(x => x.Name == name && x.Password == password);
            return user != null;
        }

        public IList<User> GetTop(int count)
        {
            throw new NotImplementedException();
        }
    }
}