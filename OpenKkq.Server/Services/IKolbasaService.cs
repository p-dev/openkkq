using System.Collections.Generic;
using OpenKkq.Server.Models;
using OpenKkq.Server.Models.Items;
using OpenKkq.Server.ViewModels;

namespace OpenKkq.Server.Services
{
    public interface IKolbasaService
    {
        void Kolbasa(User user);

        CheckResponse Check(User user);

        KolbasaKind NewKind(string name, int price);
        
        IEnumerable<Item> GetItems(User user);
    }
}