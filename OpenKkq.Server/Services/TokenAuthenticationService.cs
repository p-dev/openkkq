using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using OpenKkq.Server.Models;
using OpenKkq.Server.ViewModels;

namespace OpenKkq.Server.Services
{
    public class TokenAuthenticationService : IAuthenticateService
    {
        private readonly IUserManagementService _userManagementService;
        private readonly TokenManagement _tokenManagement;
        private readonly KkqDbContext _db;
        
        public TokenAuthenticationService(IUserManagementService userManagementService, IOptions<TokenManagement> tokenManagementOptions, KkqDbContext db)
        {
            _userManagementService = userManagementService;
            _db = db;
            _tokenManagement = tokenManagementOptions.Value;
        }

        public bool IsAuthenticated(TokenRequest request, out TokenResponse token)
        {
            token = null;

            if (!_userManagementService.IsValidUser(request.Name, request.Password, out var user))
                return false;

            var claims = new Claim[]
            {
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()), 
            };

            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_tokenManagement.Secret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var jwtToken = new JwtSecurityToken(
                _tokenManagement.Issuer,
                _tokenManagement.Audience,
                claims,
                expires: DateTime.UtcNow.AddMinutes(_tokenManagement.AccessExpiration),
                signingCredentials: credentials);
            
            token = new TokenResponse()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(jwtToken),
            };

            return true;
        }

        public User NewUser(TokenRequest request)
        {
            if (_db.Users.Any(x => x.Name == request.Name))
                return null;
            
            var newUser = new User()
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                Password = request.Password
            };

            _db.Users.Add(newUser);
            _db.SaveChanges();

            return newUser;
        }
    }
}