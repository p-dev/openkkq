using System;
using System.Collections.Generic;
using System.Linq;
using OpenKkq.Server.Models;
using OpenKkq.Server.Models.Items;
using OpenKkq.Server.ViewModels;

namespace OpenKkq.Server.Services
{
    public class KolbasaService : IKolbasaService
    {
        private readonly KkqDbContext _db;

        public KolbasaService(KkqDbContext db)
        {
            _db = db;
        }

        public void Kolbasa(User user)
        {
            var newKolbasa = new Kolbasa()
            {
                Id = Guid.NewGuid(),
                Owner = user,
                Kind = _db.KolbasaKinds.First()
            };

            _db.Items.Add(newKolbasa);
            user.Items.Add(newKolbasa);

            _db.SaveChanges();

        }

        public CheckResponse Check(User user)
        {
            var kolbCount = _db.Items.Where(x => x.Owner.Id == user.Id).OfType<Kolbasa>().Count();
            
            return new CheckResponse()
            {
                KolbCount = kolbCount,
                Meat = -1,
                Money = -1
            };
        }

        public KolbasaKind NewKind(string name, int price)
        {
            var newKind = new KolbasaKind()
            {
                Id = Guid.NewGuid(),
                Name = name,
                Price = price
            };

            _db.KolbasaKinds.Add(newKind);
            _db.SaveChanges();

            return newKind;
        }

        public IEnumerable<Item> GetItems(User user)
        {
            throw new System.NotImplementedException();
        }
    }
}