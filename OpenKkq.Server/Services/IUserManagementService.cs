using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using OpenKkq.Server.Models;
using OpenKkq.Server.ViewModels;

namespace OpenKkq.Server.Services
{
    public interface IUserManagementService
    {
        [CanBeNull] 
        User GetUserById(Guid id);

        [CanBeNull] 
        User GetUserByName(string name);

        bool IsValidUser(string name, string password, out User user);
        
        IList<User> GetTop(int count);
    }
}