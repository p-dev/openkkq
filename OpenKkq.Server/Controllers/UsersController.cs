using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OpenKkq.Server.Controllers.Extensions;
using OpenKkq.Server.Models;
using OpenKkq.Server.Services;
using OpenKkq.Server.ViewModels;

namespace OpenKkq.Server.Controllers
{
    [ApiController]
    [Authorize]
    [Route("/api/users/")]
    public class UsersController : ControllerBase
    {
        private readonly IUserManagementService _userManagementService;

        public UsersController(IUserManagementService userManagementService)
        {
            _userManagementService = userManagementService;
        }

        [AllowAnonymous]
        [HttpGet("get")]
        public ActionResult<UserViewModel> GetUser([FromQuery] Guid? id = null, [FromQuery] string name = null)
        {
            User user;
            if (id.HasValue)
                user = _userManagementService.GetUserById(id.Value);
            else if (!string.IsNullOrEmpty(name))
                user = _userManagementService.GetUserByName(name);
            else if (User.Identity.IsAuthenticated)
                user = _userManagementService.GetUserById(User.GetId());
            else
                return BadRequest();

            if (user == null)
                return NotFound();
            
            return new UserViewModel()
            {
                Id = user.Id,
                Name = user.Name
            };
        }
    }
}