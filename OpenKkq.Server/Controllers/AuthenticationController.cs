using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OpenKkq.Server.Services;
using OpenKkq.Server.ViewModels;

namespace OpenKkq.Server.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Route("/api/auth")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticateService _authorizationService;

        public AuthenticationController(IAuthenticateService authorizationService)
        {
            _authorizationService = authorizationService;
        }
        
        [HttpPost("token")]
        public ActionResult<TokenResponse> RequestToken([FromBody] TokenRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_authorizationService.IsAuthenticated(request, out var token))
                return BadRequest("Invalid Request");

            return Ok(token);
        }

        [HttpPost("new_user")]
        public IActionResult NewUser([FromBody] TokenRequest request)
        {
            var user = _authorizationService.NewUser(request);
            if (user == null)
                return BadRequest();

            return Ok();
        }
    }
}