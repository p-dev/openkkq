using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OpenKkq.Server.Controllers.Extensions;
using OpenKkq.Server.Services;
using OpenKkq.Server.ViewModels;

namespace OpenKkq.Server.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/kolbasa")]
    public class KolbasaController : ControllerBase
    {
        private readonly IUserManagementService _userManagementService;
        private readonly IKolbasaService _kolbasaService;
        
        public KolbasaController(IUserManagementService userManagementService, IKolbasaService kolbasaService)
        {
            _userManagementService = userManagementService;
            _kolbasaService = kolbasaService;
        }

        [HttpGet("check")]
        public ActionResult<CheckResponse> Check()
        {
            var user = _userManagementService.GetUserById(User.GetId());
            var checkViewModel = _kolbasaService.Check(user);
            return checkViewModel;
        }

        [HttpPost("new_kind")]
        public IActionResult NewKind([FromBody] KindViewModel kindViewModel)
        {
            var newKind = _kolbasaService.NewKind(kindViewModel.Name, kindViewModel.Price);
            return Ok();
        }

        [HttpGet]
        public IActionResult Kolbasa()
        {
            var user = _userManagementService.GetUserById(User.GetId());

            _kolbasaService.Kolbasa(user);
            
            return Ok();
        }
    }
}