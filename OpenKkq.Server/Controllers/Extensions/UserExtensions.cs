using System;
using System.Security.Claims;

namespace OpenKkq.Server.Controllers.Extensions
{
    public static class UserExtensions
    {
        public static Guid GetId(this ClaimsPrincipal user)
        {
            return Guid.Parse(user.FindFirst(ClaimTypes.NameIdentifier)?.Value);
        }
    }
}