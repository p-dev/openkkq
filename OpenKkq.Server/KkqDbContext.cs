using Microsoft.EntityFrameworkCore;
using OpenKkq.Server.Models;
using OpenKkq.Server.Models.Items;

namespace OpenKkq.Server
{
    public sealed class KkqDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<KolbasaKind> KolbasaKinds { get; set; }
        
        public KkqDbContext(DbContextOptions options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder mb)
        {
            mb.Entity<User>(builder =>
            {
                builder.HasMany(x => x.Items)
                    .WithOne(x => x.Owner);
            });

            mb.Entity<Kolbasa>();
            mb.Entity<KolbasaKind>();
        }
    }
}