using System.Net.Http;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace OpenKkq.DiscordBot.Services
{
    public interface IRequestingService
    {
        Task<HttpResponseMessage> GetAsync(string uri, [CanBeNull] string token = null);
        Task<HttpResponseMessage> PostJsonAsync(string uri, object obj, [CanBeNull] string token = null);
    }
}
