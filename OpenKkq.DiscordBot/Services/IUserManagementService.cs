using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JetBrains.Annotations;
using OpenKkq.DiscordBot.Models;
using OpenKkq.DiscordBot.ViewModels;

namespace OpenKkq.DiscordBot.Services
{
    public interface IUserManagementService
    {
        [CanBeNull]
        User GetUserByDiscordId(ulong id);

        Task<IList<User>> GetTop(int count);

        void RecordUser(Guid id, ulong discordId, string name, string password, [CanBeNull] string token, DateTime tokenExpTime);
        void RecordUserIfDont(Guid id, ulong discordId, string name, string password, [CanBeNull] string token, DateTime tokenExpTime);
    }
}