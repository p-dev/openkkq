using System;
using System.Threading.Tasks;
using OpenKkq.DiscordBot.Models;

namespace OpenKkq.DiscordBot.Services
{
    public interface IKolbasaService
    {
        Task<bool> Kolbasa(User user);
    }
}