using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using OpenKkq.DiscordBot.Models;
using OpenKkq.DiscordBot.ViewModels;

namespace OpenKkq.DiscordBot.Services
{
    public interface IAuthenticationService
    {
        Task<bool> RegisterAsync(string name, string password);
        
        Task<UserResponse> LoginAsync(string name, string password);

        bool DoesUserRecorded(ulong discordId);
        
        Task UpdateTokenIfNeedAsync(User user);
    }
}