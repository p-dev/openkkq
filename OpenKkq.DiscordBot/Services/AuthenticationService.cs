using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Newtonsoft.Json;
using OpenKkq.DiscordBot.Extensions;
using OpenKkq.DiscordBot.Models;
using OpenKkq.DiscordBot.ViewModels;

namespace OpenKkq.DiscordBot.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly KkqDiscordDbContext _db;
        private readonly IRequestingService _requestingService;

        public AuthenticationService(KkqDiscordDbContext db, IRequestingService requestingService)
        {
            _db = db;
            _requestingService = requestingService;
        }

        public async Task<bool> RegisterAsync(string name, string password)
        {
            var response = await _requestingService.PostJsonAsync("/api/auth/new_user", 
                new
                {
                    Name = name,
                    Password = password
                });

            return response.IsSuccessStatusCode;
        }

        public async Task<UserResponse> LoginAsync(string name, string password)
        {
            var response = await _requestingService.PostJsonAsync("/api/auth/token", new
            {
                Name = name,
                Password = password
            });

            if (!response.IsSuccessStatusCode)
                return null;
                
            var token = (await response.Content.ReadAsTFromJson<TokenResponse>()).Token;

            var jwtToken = (JwtSecurityToken) new JwtSecurityTokenHandler().ReadToken(token);

            var userId = Guid.Parse(
                jwtToken.Claims.FirstOrDefault(
                        x => x.Type == ClaimTypes.NameIdentifier)
                    ?.Value);

            var expTime = jwtToken.ValidTo;

            return new UserResponse()
            {
                Id = userId,
                Name = name,
                Token = token,
                TokenExpireDate = expTime
            };
        }

        public bool DoesUserRecorded(ulong discordId)
        {
            return _db.Users.Any(x => x.DiscordId == discordId);
        }

        public async Task UpdateTokenIfNeedAsync(User user)
        {
            // Token isn't expired
            if (!user.TokenExpireTime.IsExpired())
                return;

            var userViewModel = await LoginAsync(user.Name, user.Password);

            user.Token = userViewModel.Token;
            user.TokenExpireTime = userViewModel.TokenExpireDate;

            _db.Users.Update(user);
            _db.SaveChanges();
        }
    }
}