using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Newtonsoft.Json;
using OpenKkq.DiscordBot.Extensions;
using OpenKkq.DiscordBot.Models;
using OpenKkq.DiscordBot.ViewModels;

namespace OpenKkq.DiscordBot.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly KkqDiscordDbContext _db;

        private readonly IRequestingService _requestingService;
        
        public UserManagementService(KkqDiscordDbContext db, IRequestingService requestingService)
        {
            _db = db;
            _requestingService = requestingService;
        }

        public User GetUserByDiscordId(ulong id)
        {
            return _db.Users.FirstOrDefault(user => user.DiscordId == id);
        }
        
        public async Task<IList<User>> GetTop(int count)
        {
            var response = await _requestingService.GetAsync("/api/users/top");
            var users = await response.Content.ReadAsTFromJson<IList<UserResponse>>();
            return _db.Users.Where(x => users.Any(u => u.Id == x.Id)).ToList();
        }
        
        public void RecordUser(Guid id, ulong discordId, string name, string password, [CanBeNull] string token, DateTime tokenExpTime)
        {
            var user = new User()
            {
                Id = id,
                DiscordId = discordId,
                Name = name,
                Password = password,
                Token = token,
                TokenExpireTime = tokenExpTime
            };

            _db.Users.Add(user);
            _db.SaveChanges();
        }

        public void RecordUserIfDont(Guid id, ulong discordId, string name, string password, [CanBeNull] string token, DateTime tokenExpTime)
        {
            // Exists
            if (_db.Users.Any(x => x.Id == id))
                return;
            
            RecordUser(id, discordId, name, password, token, tokenExpTime);
        }
    }
}