using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;

namespace OpenKkq.DiscordBot.Services
{
    public class RequestingService : IRequestingService
    {
        private readonly HttpClient _httpClient;

        public RequestingService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<HttpResponseMessage> GetAsync(string uri, string token)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, uri);
            
            if (token != null)
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            
            return await _httpClient.SendAsync(requestMessage);
        }

        public async Task<HttpResponseMessage> PostJsonAsync(string uri, object obj, string token)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(obj), 
                Encoding.UTF8,
                MediaTypeNames.Application.Json);
            
            // Authorization
            if (token != null)
                content.Headers.Add(HeaderNames.Authorization, $"Bearer {token}");
            
            return await _httpClient.PostAsync(uri, content);
        }
    }
}