using System;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using OpenKkq.DiscordBot.Models;

namespace OpenKkq.DiscordBot.Services
{
    public class KolbasaService : IKolbasaService
    {
        private readonly IRequestingService _requestingService;
        
        public KolbasaService(IRequestingService requestingService)
        {
            _requestingService = requestingService;
        }

        public async Task<bool> Kolbasa(User user)
        {
            var response = await _requestingService.GetAsync("/api/kolbasa", user.Token);
            return response.IsSuccessStatusCode;
        }
    }
}