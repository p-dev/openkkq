using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net.BotMvc.CommandView;
using OpenKkq.DiscordBot.Extensions;
using OpenKkq.DiscordBot.Services;
using OpenKkq.DiscordBot.ViewModels;

namespace OpenKkq.DiscordBot.Modules
{
    public class UserModule : ModuleBase
    {
        private readonly IUserManagementService _userManagementService;

        private readonly IRequestingService _requestingService;
        
        public UserModule(IUserManagementService userManagementService, IRequestingService requestingService)
        {
            _userManagementService = userManagementService;
            _requestingService = requestingService;
        }

        [Command("чек")]
        public async Task<RuntimeResult> CheckAsync()
        {
            var user = _userManagementService.GetUserByDiscordId(Context.User.Id);
            if (user == null)
                return new ErrorResult("отказ");

            var response = await _requestingService.GetAsync("/api/kolbasa/check", user.Token);

            var checkResponse = await response.Content.ReadAsTFromJson<CheckResponse>();
            
            return new StringResult($"{MentionUtils.MentionUser(user.DiscordId)}\n{checkResponse.KolbCount} колбасы");
        }

        [Command("лучшие")]
        public async Task<RuntimeResult> TopAsync(int count = 10)
        {
            var users = await _userManagementService.GetTop(count);
            var sb = new StringBuilder();
            sb.AppendLine("лучшие");
            
            var i = 0;
            foreach (var user in users)
            {
                i++;
                sb.AppendLine($"\t{i} | {user} {MentionUtils.MentionUser(user.DiscordId)}");
            }
            
            return new StringResult(sb.ToString());
        }
    }
}