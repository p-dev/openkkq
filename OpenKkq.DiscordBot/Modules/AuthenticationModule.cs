using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net.BotMvc.CommandView;
using OpenKkq.DiscordBot.Extensions.Preconditions;
using OpenKkq.DiscordBot.Services;

namespace OpenKkq.DiscordBot.Modules
{
    public class AuthenticationModule : ModuleBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserManagementService _userManagementService;

        public AuthenticationModule(IAuthenticationService authenticationService, IUserManagementService userManagementService)
        {
            _authenticationService = authenticationService;
            _userManagementService = userManagementService;
        }

        [Command("регистрация")]
        [OnlyDmPrecondition(ErrorMessage = "регистрируйбся в личке")]
        public async Task<RuntimeResult> RegisterAsync(string name, string password)
        {
            var success = await _authenticationService.RegisterAsync(name, password);
            if (!success)
                return new ErrorResult("отказ");

            return new StringResult("зарегистрирован");
        }
        
        [Command("логин")]
        [OnlyDmPrecondition(ErrorMessage = "логинбся в личке")]
        public async Task<RuntimeResult> LoginAsync(string name, string password)
        {
            var userResponse = await _authenticationService.LoginAsync(name, password);
            
            if (userResponse == null)
                return new ErrorResult("отказ");

            _userManagementService.RecordUserIfDont(userResponse.Id, Context.User.Id, name, password, userResponse.Token, userResponse.TokenExpireDate);

            return new EmbedResult(new EmbedBuilder()
                .WithDescription($"вошол как {userResponse.Name}")
                .WithColor(Color.Green)
                .Build());
        }
    }
}