using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net.BotMvc.CommandView;
using OpenKkq.DiscordBot.Extensions;
using OpenKkq.DiscordBot.Extensions.Preconditions;
using OpenKkq.DiscordBot.Services;

namespace OpenKkq.DiscordBot.Modules
{
    public class KolbasaModule : ModuleBase
    {
        private readonly IKolbasaService _kolbasaService;
        private readonly IUserManagementService _userManagementService;
        
        public KolbasaModule(IKolbasaService kolbasaService, IUserManagementService userManagementService)
        {
            _kolbasaService = kolbasaService;
            _userManagementService = userManagementService;
        }

        [Command("колбаса")]
        [Alias("rjk,fcf", "kolbasa")]
        [RequireToken]
        public async Task<RuntimeResult> KolbasaAsync()
        {
            var user = _userManagementService.GetUserByDiscordId(Context.User.Id);

            var success = await _kolbasaService.Kolbasa(user);

            if (success)
                return new DebugResult("колбаса");
            else
                return new DebugResult("не колбаса");
        }
    }
}