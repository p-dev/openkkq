using Discord;
using Discord.Net.BotMvc.CommandView;

namespace OpenKkq.DiscordBot.Extensions
{
    public class DebugResult : EmbedResult
    {
        public DebugResult(string text) : base(
            new EmbedBuilder()
                .WithColor(Color.DarkPurple)
                .WithDescription(text)
                .WithFooter("Debug")
                .Build())
        {
        }
    }
}