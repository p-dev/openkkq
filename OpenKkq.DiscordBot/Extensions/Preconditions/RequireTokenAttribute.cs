using System;
using System.Threading.Tasks;
using Discord.Commands;
using Microsoft.Extensions.DependencyInjection;
using OpenKkq.DiscordBot.Services;

namespace OpenKkq.DiscordBot.Extensions.Preconditions
{
    public sealed class RequireTokenAttribute : PreconditionAttribute
    {
        public RequireTokenAttribute()
        {
           ErrorMessage = "залогинься";
        }
    
        public override string ErrorMessage { get; set; }

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            var authenticationService = services.GetRequiredService<IAuthenticationService>();
            var userManagementService = services.GetRequiredService<IUserManagementService>();
            
            var userDiscordId = context.User.Id;

            var errorResult = Task.FromResult(PreconditionResult.FromError(ErrorMessage));
            
            var user = userManagementService.GetUserByDiscordId(userDiscordId);

            if (user == null)
                return errorResult;
            
            if (user.TokenExpireTime.IsExpired())
                return errorResult;
            
            authenticationService.UpdateTokenIfNeedAsync(user);

            return Task.FromResult(PreconditionResult.FromSuccess());
        }
    }
}