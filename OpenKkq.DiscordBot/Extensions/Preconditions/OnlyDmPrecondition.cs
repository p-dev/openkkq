using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net.BotMvc.CommandView;

namespace OpenKkq.DiscordBot.Extensions.Preconditions
{
    public class OnlyDmPrecondition : PreconditionAttribute
    {
        public override string ErrorMessage { get; set; }

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            if (context.Channel is IDMChannel)
            {
                return Task.FromResult(PreconditionResult.FromSuccess());
            }
            else
            {
                return Task.FromResult(PreconditionResult.FromError(new ErrorResult(ErrorMessage)));
            }
        }
    }
}