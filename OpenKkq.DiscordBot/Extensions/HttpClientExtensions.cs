using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OpenKkq.DiscordBot.Extensions
{
    public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> PostJsonAsync(this HttpClient httpClient, string uri, object obj)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(obj), 
                Encoding.UTF8, 
                MediaTypeNames.Application.Json);
            
            return httpClient.PostAsync(uri, content);
        }
        
        public static async Task<TResult> ReadAsTFromJson<TResult>(this HttpContent httpContent)
        {
            var str = await httpContent.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TResult>(str);
        }
    }
}