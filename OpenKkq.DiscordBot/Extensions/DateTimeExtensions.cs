using System;

namespace OpenKkq.DiscordBot.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsExpired(this DateTime dateTime)
        {
            var gap = dateTime - DateTime.Now;
            return gap.Milliseconds > 0;
        }
    }
}