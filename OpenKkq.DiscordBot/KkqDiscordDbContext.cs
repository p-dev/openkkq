using Microsoft.EntityFrameworkCore;
using OpenKkq.DiscordBot.Models;

namespace OpenKkq.DiscordBot
{
    public sealed class KkqDiscordDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public KkqDiscordDbContext(DbContextOptions options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder mb)
        {
            
        }
    }
}