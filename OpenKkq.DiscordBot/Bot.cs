using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net.BotMvc;
using Discord.Net.BotMvc.CommandView;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenKkq.DiscordBot.Services;

namespace OpenKkq.DiscordBot
{
    public class Bot : BotBase
    {
        private readonly IConfiguration _config;
        
        public Bot(IConfiguration config) 
            : base(ParseToken(config["Token"]), config["Prefix"], 
                new DiscordSocketConfig()
                {
                    LogLevel = LogSeverity.Debug,
                }, 
                new CommandServiceConfig()
                {
                    LogLevel = LogSeverity.Debug,
                    DefaultRunMode = RunMode.Sync,
                    CaseSensitiveCommands = false
                })
        {
            _config = config;
        }

        private static string ParseToken(string input)
        {
            return input.StartsWith("file://") 
                ? File.ReadAllText(input.Substring(7)) 
                : input;
        }

        protected override void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IKolbasaService, KolbasaService>();
            services.AddTransient<IUserManagementService, UserManagementService>();
            services.AddTransient<IAuthenticationService, AuthenticationService>();

            services.AddHttpClient<IRequestingService, RequestingService>()
                .ConfigurePrimaryHttpMessageHandler(serviceProvider => new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback =
                        (sender, cert, chain, sslPolicyErrors) => true
                })
                .ConfigureHttpClient(client =>
                {
                    client.BaseAddress = new Uri(_config["OpenKkqUrl"]);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));
                });
            
            // Db
            
            var connectionString = _config.GetConnectionString("KqDbSqlite");
            services.AddDbContext<KkqDiscordDbContext>(builder => builder.UseSqlite(connectionString));
        }

        protected override void Configure(IBotBuilder builder)
        {
            builder.Client.Log += ConsoleLog;
            builder.Commands.Log += ConsoleLog;
            builder.Client.Ready += OnClientReady;

            builder.UseViews(async (command, context, result) =>
            {
                var userPrunkles = builder.Client.GetUser(262987432209678337ul);
                
                var eb = new EmbedBuilder()
                    .WithColor(Color.Red)
                    .WithTitle("ошибка")
                    .WithDescription(result.ErrorReason);
                
                await context.Channel.SendMessageAsync(embed: eb.Build());
                
                // Send error to fucking developer
                if (context.Channel is IDMChannel dmChannel)
                {
                    if (dmChannel.Recipient != userPrunkles)
                    {
                        await userPrunkles.SendMessageAsync(embed: eb
                            .AddField("от", context.User.Mention)
                            .AddField("в", DateTime.Now)
                            .AddField("при", context.Message.Content)
                            .Build());
                    }
                }
                else
                {
                    await userPrunkles.SendMessageAsync(embed: eb
                        .AddField("от", context.User.Mention)
                        .AddField("в", DateTime.Now)
                        .AddField("при", context.Message.Content)
                        .Build());
                }
            });
        }

        private Task OnClientReady()
        {
            return Task.CompletedTask;
        }

        private Task ConsoleLog(LogMessage arg)
        {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }
    }
}