using System;

namespace OpenKkq.DiscordBot.ViewModels
{
    public class UserResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public DateTime TokenExpireDate { get; set; }
    }
}