namespace OpenKkq.DiscordBot.ViewModels
{
    public class CheckResponse
    {
        public int KolbCount { get; set; }
        
        public int Money { get; set; }
        
        public int Meat { get; set; }
    }
}