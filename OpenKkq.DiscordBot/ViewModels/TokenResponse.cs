namespace OpenKkq.DiscordBot.ViewModels
{
    public class TokenResponse
    {
        public string Token { get; set; }
    }
}