using System;
using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;

namespace OpenKkq.DiscordBot.Models
{
    public class User
    {
        [Key]
        public Guid Id { get; set; }
        
        public ulong DiscordId { get; set; }
        
        public string Name { get; set; }
        public string Password { get; set; }
        
        [CanBeNull] 
        public string Token { get; set; }
        
        public DateTime TokenExpireTime { get; set; }
    }
}